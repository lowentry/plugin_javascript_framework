**Low Entry Javascript Framework** was a **Javascript framework** for web applications that will run completely in the browser.

The functionality of the framework has been reworked to be built on top of the Gatsby framework instead.

This project is an example of how to set up a Gatsby project to work the same was as the Low Entry Javascript Framework did.


### Plugins

- [gatsby-plugin-automatic-importer][1] - this plugin automates the import statements within your project.
- [@lowentry/react-redux][2] - this plugin provides the improved Redux functionality.
- [@lowentry/utils][3] - this plugin provides a collection of useful utility functions.

These plugins provide all the functionality that was previously available in the Low Entry Javascript Framework.


[1]: https://www.npmjs.com/package/gatsby-plugin-automatic-importer

[2]: https://www.npmjs.com/package/@lowentry/react-redux

[3]: https://www.npmjs.com/package/@lowentry/utils
