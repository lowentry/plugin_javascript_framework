import React from 'react';


export const Head = () => (<>
	<html lang="en"/>
	<title>MCM Homes</title>
	<meta charSet="utf-8"/>
	<meta name="description" content="Try out the MCM Homes web-based home builder now! Customize your home, choose your options and variations, and find your dream home!"/>
	<meta name="color-scheme" content="dark"/>
	<meta name="viewport" content="user-scalable=no, initial-scale=0.75, maximum-scale=0.75, minimum-scale=0.75, width=device-width, height=device-height"/>
</>);
