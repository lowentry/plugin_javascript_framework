import React from 'react';
import {LeRed} from '@lowentry/react-redux';
import {createTheme} from '@mui/material';
import {brown} from '@mui/material/colors';
import {LoadingSpinnerWidget, MuiRoot} from '@lowentry/mui';


export const Main = LeRed.memo(({children, className, ...other}) =>
{
	const baseTheme = createTheme({});
	const theme = createTheme({
		spacing:   factor => `${factor * 0.5}rem`,
		typography:{
			htmlFontSize:16,
		},
		palette:   {
			mode:     'dark',
			primary:  {main:'#008FCE'},
			secondary:brown,
		},
	});
	
	return (<>
		<MuiRoot theme={theme}>
			<main>
				<div className={'app ' + (className ?? '')} {...other}>
					<LoadingSpinnerWidget type="transparent"/>
					<LoadingSpinnerWidget type="opaque"/>
					{children}
				</div>
			</main>
		</MuiRoot>
	</>);
});
