import {LeRed} from '@lowentry/react-redux';
import {FLOAT_ANY} from '@lowentry/utils';


export const stateTimer = LeRed.createSlice
({
	state:
		{
			counter:0,
		},
	actions:
		{
			reset:
				(state) =>
				{
					state.counter = 0;
				},
			
			increase:
				(state, data) =>
				{
					state.counter += FLOAT_ANY(data, 1);
				},
			
			decrease:
				(state, data) =>
				{
					state.counter -= FLOAT_ANY(data, 1);
				},
			
			waitAndIncrease:
				function* (data)
				{
					const seconds = FLOAT_ANY(data, 1);
					yield LeRed.effects.delay(seconds * 1000);
					yield LeRed.effects.put(stateTimer.actions.increase(seconds));
				},
		},
	selectors:
		{
			counter:
				state => state.counter,
			
			valueOfWholeState:
				function(state)
				{
					// If you use a function(){} instead of a () => {}, you'll be able to use "this",
					//   which will always refer to the entire state (rather than just this slice of the state).
					// This will also work for actions (such as "increase" and "decrease" shown above).
					return this;
				},
		},
});
