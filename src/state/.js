import {stateExample} from './example';
import {stateTimer} from './timer';


export const AppStateSlices = {
	stateExample,
	stateTimer,
};
