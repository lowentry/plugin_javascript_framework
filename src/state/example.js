import {LeRed} from '@lowentry/react-redux';
import {TEST_ONE_ACTION} from './actions/example';


export const stateExample = LeRed.createSlice
({
	state:
		{
			example:{
				id:    0,
				email: '',
				fields:{
					aaa:false,
					bbb:0,
				},
			},
		},
	actions:
		{
			set:
				(state, data) =>
				{
					state.example = data;
				},
			
			update:
				(state, data) =>
				{
					state.example = {
						...state.example,
						...data,
						fields:{
							...state.example.fields,
							...(data.fields ?? {}),
						},
					};
				},
			
			[TEST_ONE_ACTION]:// supports LeRed.createAction() objects, which makes it possible to listen to the same action in multiple slices, call it like:  dispatch(SOME_ACTION('some data'))  instead of doing:  dispatch(someSlice.actions.someAction('some data'))
				(state, data) =>
				{
					console.log(data);
				},
			
			//@ts-ignore: TS2464
			[['testTwo', 'testThree']]:// support for multiple actions for a given reducer/saga
				(state, data) =>
				{
					console.log(data);
				},
			
			testFour:
				[
					// support for multiple reducers/sagas for a given action
					(state, data) =>
					{
						console.log(data);
					},
					(state, data) =>
					{
						console.log(data);
					},
					function* (data)
					{
						yield console.log(data);
					},
					function* (data)
					{
						yield console.log(data);
					},
				],
			
			//@ts-ignore: TS2464
			[['testFive', 'testSix']]:// supports both multiple actions...
				[
					// ...as well as multiple reducers/sagas
					(state, data) =>
					{
						console.log(data);
					},
					function* (data)
					{
						yield console.log(data);
					},
				],
		},
	selectors:
		{
			id:
				state => state.example.id,
			
			email:
				state => state.example.email,
			
			fieldAaa:
				state => state.example.fields.aaa,
			
			fieldBbb:
				state => state.example.fields.bbb,
			
			idAndEmail:// when wanting to use other selectors, you can do so with the LeRed.createSelector() function, the last function will receive the result of the previous functions (or values)
				LeRed.createSelector(state => [
					state.example.id, // simply retrieves the ID from the state of this slice
					stateExample.selectors.email, // runs the selector, which uses the state of the slice the selector is in, which allows you to access data from other slice states
					states => states.stateExample.example.id, // also allows you to access states from other slices, should however be avoided since slice names can easily be changed, and fixing/updating all references then will be a pain, best to stick to using selectors from other slices if wanting to access the state from another slice
					(id, email, idAsWell) => 'ID=' + id + ' email=' + email + ' ID2=' + idAsWell,
				]),
		},
});
