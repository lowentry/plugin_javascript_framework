import {React, LeRed, LeUtils, ISSET, IS_ARRAY, ARRAY, IS_OBJECT, OBJECT, STRING, STRING_ANY, INT, INT_ANY, FLOAT, FLOAT_ANY, INT_LAX, INT_LAX_ANY, FLOAT_LAX, FLOAT_LAX_ANY, TEST_ONE_ACTION, stateExample, stateTimer, AppStateSlices} from './../../imports.js';
import {StaticImage} from 'gatsby-plugin-image';
import {AppTimer} from './AppTimer';


export const App = LeRed.memo(({}) =>
{
	return (<>
		<StaticImage src="../images/logo.png" alt="" width={100} height={100} placeholder="none"/><br/>
		<br/>
		<AppTimer/>
	</>);
});
