import {React, LeRed, LeUtils, ISSET, IS_ARRAY, ARRAY, IS_OBJECT, OBJECT, STRING, STRING_ANY, INT, INT_ANY, FLOAT, FLOAT_ANY, INT_LAX, INT_LAX_ANY, FLOAT_LAX, FLOAT_LAX_ANY, TEST_ONE_ACTION, stateExample, stateTimer, AppStateSlices} from './../../imports.js';
import {Button} from '@mui/material';


export const AppTimer = LeRed.memo(({}) =>
{
	const dispatch = LeRed.useDispatch();
	const counter = LeRed.useSelector(stateTimer.selectors.counter);
	const previousCounter = LeRed.usePrevious(counter);
	
	
	LeRed.useEffectInterval(() =>
	{
		dispatch(stateTimer.actions.increase(1));
	}, [], 1000);
	
	
	return (
		<div>
			<div>
				<div>Seconds: {counter}</div>
				{ISSET(previousCounter) && (<div>Previously: {previousCounter}</div>)}
			</div>
			<Button color="primary" variant="contained" size="small" onClick={() => dispatch(stateTimer.actions.reset())}>Reset</Button>
		</div>
	);
});
