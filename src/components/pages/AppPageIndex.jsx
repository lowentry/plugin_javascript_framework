import {React, LeRed, LeUtils, ISSET, IS_ARRAY, ARRAY, IS_OBJECT, OBJECT, STRING, STRING_ANY, INT, INT_ANY, FLOAT, FLOAT_ANY, INT_LAX, INT_LAX_ANY, FLOAT_LAX, FLOAT_LAX_ANY, TEST_ONE_ACTION, stateExample, stateTimer, AppStateSlices} from './../../../imports.js';
import {App} from '../App';
import {LoadingSpinner} from '@lowentry/mui';


export const AppPageIndex = LeRed.memo(({}) =>
{
	const [store, setStore] = LeRed.useState(null);
	
	
	LeRed.useEffect(() => LeUtils.onDomReady(() =>
	{
		const store = LeRed.configureStore({slices:AppStateSlices});
		setStore(store);
	}).remove, []);
	
	
	if(!store)
	{
		return (<LoadingSpinner type="transparent"/>);
	}
	return (<>
		<LeRed.Root store={store}>
			<App/>
		</LeRed.Root>
	</>);
});
