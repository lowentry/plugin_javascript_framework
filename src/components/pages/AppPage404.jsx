import {React, LeRed, LeUtils, ISSET, IS_ARRAY, ARRAY, IS_OBJECT, OBJECT, STRING, STRING_ANY, INT, INT_ANY, FLOAT, FLOAT_ANY, INT_LAX, INT_LAX_ANY, FLOAT_LAX, FLOAT_LAX_ANY, TEST_ONE_ACTION, stateExample, stateTimer, AppStateSlices} from './../../../imports.js';
import {navigate} from 'gatsby';


export const AppPage404 = LeRed.memo(({}) =>
{
	navigate('/');
	return null;
});
