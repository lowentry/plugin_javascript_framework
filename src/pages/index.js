import '../../imports.css.js';
import React from 'react';
import {LeRed} from '@lowentry/react-redux';
import {Main} from '../components-ssr/Main';
import {LoadingSpinner} from '@lowentry/mui';


export {Head} from '../components-ssr/Head';


const LoadingComponent = LeRed.PreloadComponent(() => import('../components/pages/AppPageIndex.jsx'));
const Page = LeRed.memo(({params}) => (
	<Main>
		<LeRed.LoadComponent {...params} loading={<LoadingSpinner type="transparent"/>} load={() => LoadingComponent().then(imports => imports.AppPageIndex)}/>
	</Main>
));
export default Page;
