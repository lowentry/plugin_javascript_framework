import {Plugins} from '@lowentry/gatsby/gatsby-config.mjs';


const config = {
	flags:       {
		DEV_SSR:true,
	},
	siteMetadata:{
		title:  'My Site',
		siteUrl:'https://www.yourdomain.tld',
	},
	plugins:     [
		...Plugins({
			manifestOptions:{
				name:            'MCM Homes',
				short_name:      'MCM Homes',
				background_color:'#1A1A1A',
				theme_color:     '#193E81',
			},
			precachePages:  [],
		}),
		{
			resolve:'gatsby-plugin-automatic-importer',
			options:{
				'import':          [
					'./src/css/',
					'./src/components/',
				],
				'fileExtensionsJs':[],
				'outputName':      'imports.css.js',
			},
		},
		{
			resolve:'gatsby-plugin-automatic-importer',
			options:{
				'import':             [
					'./src/_core/',
					'./src/components-ssr/widgets/',
					'./src/components/widgets/',
					'./src/state/',
				],
				'modify':             [
					'./src/components/',
				],
				'fileExtensionsOther':[],
				'outputName':         'imports.js',
			},
		},
	],
};
export default config;
